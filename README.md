# zmap-easy
A script for easily scanning ports on the Internet with zmap.
## What is it?
A Python script that initialises zmap for you, making it easier and more approachable.
## 30-Second Demo
[![asciicast](https://asciinema.org/a/9wPYznMpO4GiGoQyTUd2WBXJx.svg)](https://asciinema.org/a/9wPYznMpO4GiGoQyTUd2WBXJx)
## Features
- [x] Support for multiple ports:
    + [x] Port 80
    + [x] Port 443
    + [x] Port 21
    + [x] Port 22
    + [x] Port 25
    + [x] Port 443
    + [x] Port 5900
    + [x] Port 6881
- [x] Multi-threaded execution
- [x] Easy selection of IP address range 
    + [x] Select start IP + range (`/8`, `/16`, or `/24`)
- [ ] IPv6 Support
- [x] Logging of IP addresses to text file
- [ ] Coloured terminal text
- [ ] TOML Config file
## Running
Simply run:
```
$ python main.py
```
## F.A.Q
**Where does it save IP addresses?**

IP addresses are saved in a new folder determined by IP address range, e.g a range of `100.0.0.0` to `100.0.255.255` would save to `100.0.x.x`. Each zmap instance saves to a file titled `<port num>.txt`.

**Will *feature X* be implemented?**

Maybe. If it's on the "Features" list, I might have a go at it.

**Why did you make this?**

I realised that zmap doesn't support multiple ports, so I decided to make a script that executes everything for my in a simple yet customisable manner.

**Do you plan on adding a "Scan the entire Internet" mode?

No. I don't think I should condone **that** size of port scanning. Also it would take a stupidly long time (zmap estimated 4 days for me), and zmap takes up basically all your Internet traffic, and the cost of running a computer for so long is quite high, and the cost of being offline is even greater than that for some people. So, no.

## Troubleshooting
**[FATAL] recv: could not open device <DEVICE>: <DEVICE>: You don't have permission to perform this capture on that device (socket: Operation not permitted)**

Run `main.py` as root or use `sudo`.

**[FATAL] csv: could not open CSV output file (<FILE>): Permission denied**

This error occurs when running `main.py` as a regular user, and choosing the same options as a previous scan. Run it as root or with `sudo` and try again.

**I don't have write access to the logs/the folders**

This is because you ran it either as root or with sudo, and the root user created the file.

To fix this:

```
chown <username> <file or folder>
```

## Contributing
Contributions are welcome. If you're new to open-source development, we especially welcome your additions! After all, your first commit might not be important to a project, but it will be important to you.

### Create a pull request
1. Fork the project;
2. Make a change;
3. Make a pull request;
4. See if your change gets merged in!

### Open an issue
1. Find a bug/idea;
1. Open an issue;
2. See if someone bothers to fix/implement your bug/idea.

## License
This script is licensed under the **MIT License**. 
