import sys
import platform
import os
import concurrent.futures

def execute(command):
    os.system(command)

def clear():
    os.system("clear")


def div():
    print("--------------------")


def br():
    div()
    print("Press ENTER to continue.")
    return input()


def main():
    if platform.uname()[0] == "Windows":
        return "FATAL ERROR: This tool does not work on Windows.\nEither:\n1. Use WSL\n2. Get Ubuntu or similar and use it there."
    PORTS = []
    clear()
    div()
    print("Welcome to zmap-easy.")
    print("This tool creates multiple zmap instances and allows you to easily scan the Internet for devices with open ports.")
    div()
    print("PART ONE: ADDRESS RANGE")
    div()
    print("Enter the starting IPv4 address, e.g. 100.0.0.0")
    div()
    start = input("Choose (Default 100.0.0.0): ")
    if not start:
        start = "100.0.0.0"
    start = start.split(".")
    try:
        start = [int(x) for x in start]
    except:
        return "ERROR: IPv4 addresss can only contain numbers"
    if len(start) != 4:
        return "ERROR: IPv4 Address must be in the format x.x.x.x"
    for x in start:
        if x > 255:
            return "ERROR: Values in the IPv4 address cannot be > 255"
        if x < 0:
            return "ERROR: Values in IPv4 address cannot be < 0"
    clear()
    ADDRESS = ".".join([str(x) for x in start])

    HITRATE = 0.0011 ## 0.11% (based on experience)
    HITS_OPT1 = int(16777216 * HITRATE)
    HITS_OPT2 = int(65536 * HITRATE)
    HITS_OPT3 = int(256 * HITRATE)
    
    div()
    print("Select what address range zmap should use:")
    div()
    print("[1] {} --> {}.{}.{}.{}".format(ADDRESS, start[0], 255, 255, 255))
    print("    Addresses: 16,777,216")
    print("    Time: ~1 hour")
    print("    Hits (port 80): ~{}".format(HITS_OPT1))
    print("[2] {} --> {}.{}.{}.{}".format(ADDRESS, start[0], start[1], 255, 255))
    print("    Addresses: 65,536")
    print("    Time ~30 seconds")
    print("    Hits (port 80): ~{}".format(HITS_OPT2))
    print("[3] {} --> {}.{}.{}.{}".format(ADDRESS, start[0], start[1], start[2], 255))
    print("    Addresses: 256")
    print("    Time: ~1 second")
    print("    Hits (port 80): ~{}".format(HITS_OPT3))
    div()
    ch = input("Choose (Default 2): ")
    if ch == "1":
        MODE = "/8"
    elif ch == "2":
        MODE = "/16"
    elif ch == "3":
        MODE = "/24"
    else:
        MODE = "/16" ## Default to #2

    if MODE == "/8":
        FOLDER = "{}.x.x.x".format(start[0])
    elif MODE == "/16":
        FOLDER = "{}.{}.x.x".format(start[0], start[1])
    elif MODE == "/24":
        FOLDER = "{}.{}.{}.x".format(start[0], start[1], start[2])

    print(FOLDER)

    running = True

    port_80 = True
    port_22 = False
    port_443 = False
    port_21 = False
    port_25 = False
    port_5900 = False
    port_6881 = False
    port_6969 = False

    while running:
        clear()
        div()
        print("PART TWO: PORTS")
        div()
        print("Common")
        div()
        print("[1] Port 80 (HTTP) ({})".format("Selected" if port_80 else "Deselected"))
        print("[2] Port 22 (SSH) ({})".format("Selected" if port_22 else "Deselected"))
        print("[3] Port 443 (HTTPS) ({})".format("Selected" if port_443 else "Deselected"))
        print("[4] Port 21 (FTP) ({})".format("Selected" if port_21 else "Deselected"))
        div()
        print("Uncommon")
        div()
        print("[5] Port 25 (Mail Server) ({})".format("Selected" if port_25 else "Deselected"))
        print("[6] Port 5900 (Tor) ({})".format("Selected" if port_5900 else "Deselected"))
        print("[7] Port 6881 (BitTorrent) ({})".format("Selected" if port_6881 else "Deselected"))
        print("[8] Port 6969 (BitTorrent ({})".format("Selected" if port_6969 else "Deselected"))
        div()
        print("[0] Finish (Start zmap)")
        div()
        ch = input("Choose: ")
        if ch == "1":
            port_80 = not port_80
        elif ch == "2":
            port_22 = not port_22
        elif ch == "3":
            port_443 = not port_443
        elif ch == "4":
            port_21 = not port_21
        elif ch == "5":
            port_25 = not port_25
        elif ch == "6":
            port_5900 = not port_5900
        elif ch == "7":
            port_6881 = not port_6881
        elif ch == "8":
            port_6969 = not port_6969
        elif ch == "0":
            running = False

    PORTS = []
    if port_80:
        PORTS.append(80)
    if port_21:
        PORTS.append(21)
    if port_22:
        PORTS.append(22)
    if port_443:
        PORTS.append(443)
    if port_6969:
        PORTS.append(6969)
    if port_25:
        PORTS.append(25)
    if port_6881:
        PORTS.append(6881)
    if port_5900:
        PORTS.append(5900)


    
    if not os.path.isdir(FOLDER):
        os.mkdir(FOLDER)

    COMMANDS = []
    for port in PORTS:
        COMMANDS.append("exec zmap  {}{} -p {} -o {}/{}.txt".format(ADDRESS, MODE, port, FOLDER, port))

    with concurrent.futures.ThreadPoolExecutor() as executor:
        executor.map(execute, COMMANDS)

    RESULTS = 0
    for PORT in PORTS:
        with open("{}/{}.txt".format(FOLDER, PORT)) as f:
            RESULTS += len(f.read())

    return "Success! {} results saved to folder: {}".format(RESULTS, FOLDER) ## Successfully executed program


print(main())
